# Siggraph 2019/2020 OptiX 7/7.1 Course Tutorial Code

# PROJECT MOVED TO GITHUB.

TLDR: Due to popular demand this project was moved to gitHUB, 
in particular to https://github.com/ingowald/optix7course . 

All future updates, bug fixes, issue tracking, etc, will 
happen only on the github repo.


